# TODO: remove this as submodules aren't used anymore.
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  LIGO_REGISTRY_IMAGE_BASE_NAME: "${CI_PROJECT_PATH}/${CI_PROJECT_NAME}"
  WEBIDE_IMAGE_NAME: "registry.gitlab.com/${CI_PROJECT_PATH}/ligo_webide"

stages:
  - build
  - push
  - ide-deploy
  - versioning

.docker-image:
  stage: push
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind

version_scheduled_job:
  stage: versioning
  script:
    - scripts/versioning.sh
  only:
    - schedules

.nix:
  stage: build
  tags:
    - nix
  before_script:
    - find "$CI_PROJECT_DIR" -path "$CI_PROJECT_DIR/.git" -prune -o "(" -type d -a -not -perm -u=w ")" -exec chmod --verbose u+w {} ";"
    - nix-env -f channel:nixos-unstable -iA gnutar gitMinimal
    - export COMMIT_DATE="$(git show --no-patch --format=%ci)"

# The binary produced is useless by itself
binary:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-bin

deb:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - nix-build nix -A ligo-deb
    - cp -L result/ligo.deb .
  artifacts:
    paths:
      - ligo.deb

doc:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-doc
    - cp -Lr --no-preserve=mode,ownership,timestamps result/share/doc .
  artifacts:
    paths:
      - doc

test:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-coverage
    - cat result/share/coverage-all
    - cp -Lr --no-preserve=mode,ownership,timestamps result/share/coverage .
  artifacts:
    paths:
      - coverage

xrefcheck:
  extends: .nix
  only:
    - merge_requests
  script:
    # Should be replaced with
    # nix run github:serokell/xrefcheck
    # Once flakes roll out to stable
    # - nix run -f https://github.com/serokell/xrefcheck/archive/v0.1.1.2.tar.gz -c 'xrefcheck local-only'
    - curl -L https://github.com/serokell/xrefcheck/releases/download/v0.1.1/release.tar.gz | tar -zxf - xrefcheck/bin/xrefcheck
    - xrefcheck/bin/xrefcheck

# Strange race conditions, disable for now
.webide-e2e:
  extends: .nix
  only:
    # Disabled for now unless the branch name contains webide, because a test in this job fails randomly
    - /.*webide.*/
    #- merge_requests
    #- dev
    #- /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-editor.e2e

docker:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - nix-build nix -A ligo-docker
    - cp -L result ligo.tar.gz
  artifacts:
    paths:
      - ligo.tar.gz

docker-large:
  extends: docker
  script:
    - nix-build nix -A ligo-docker-large
    - cp -L result ligo.tar.gz

docker-push:
  extends: .docker-image
  dependencies:
    - docker
  needs:
    - docker
  only:
    - dev
    - /^.*-run-dev$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - echo ${LIGO_REGISTRY_PASSWORD} | docker login -u ${LIGO_REGISTRY_USER} --password-stdin
    - docker load -i=./ligo.tar.gz
    - if test "$CI_COMMIT_REF_NAME" = "dev"; then export LIGO_TAG=next; else if echo "$CI_COMMIT_TAG" | grep -E "[0-9]+\.[0-9]+\.[0-9]+"; then export LIGO_TAG="$CI_COMMIT_TAG"; else export LIGO_TAG=next-attempt; fi; fi
    - export LIGO_REGISTRY_FULL_NAME="${LIGO_REGISTRY_IMAGE_BUILD:-ligolang/ligo}:$LIGO_TAG"
    - docker tag ligo "${LIGO_REGISTRY_FULL_NAME}"
    - docker push "${LIGO_REGISTRY_FULL_NAME}"

docker-large-push:
  extends: docker-push
  dependencies:
    - docker-large
  needs:
    - docker-large
  script:
    - echo "${LIGO_REGISTRY_PASSWORD}" | docker login -u "${LIGO_REGISTRY_USER}" --password-stdin
    - docker load -i=./ligo.tar.gz
    - if test "$CI_COMMIT_REF_NAME" = "dev"; then export LIGO_TAG=next-large; else if echo "$CI_COMMIT_TAG" | grep -E "[0-9]+\.[0-9]+\.[0-9]+"; then export LIGO_TAG="$CI_COMMIT_TAG-large"; else export LIGO_TAG=next-attempt-large; fi; fi
    - export LIGO_REGISTRY_FULL_NAME="${LIGO_REGISTRY_IMAGE_BUILD:-ligolang/ligo}:$LIGO_TAG"
    - docker tag ligo "${LIGO_REGISTRY_FULL_NAME}"
    - docker push "${LIGO_REGISTRY_FULL_NAME}"

webide-docker:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
  script:
    - nix-build nix -A ligo-editor-docker
    - cp -L result webide.tar.gz
  artifacts:
    paths:
      - webide.tar.gz

webide-push:
  extends: .docker-image
  dependencies:
    - webide-docker
  needs:
    - webide-docker
  rules:
    # Only deploy docker when from the dev branch AND on the canonical ligolang/ligo repository
    - if: '$CI_COMMIT_REF_NAME =~ /^(dev|.*-run-dev)$/ && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always
  script:
    - echo "${CI_BUILD_TOKEN}" | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    - docker load -i=./webide.tar.gz
    - docker tag ligo-editor "${WEBIDE_IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}"
    - docker push "${WEBIDE_IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}"

deploy-handoff:
  # Handoff deployment duties to private repo
  stage: ide-deploy
  variables:
    IDE_DOCKER_IMAGE: "registry.gitlab.com/${CI_PROJECT_PATH}/ligo_webide"
    LIGO_COMMIT_REF_NAME: "${CI_COMMIT_SHORT_SHA}"
  trigger: ligolang/ligo-webide-deploy
  rules:
    # Only deploy handoff when from the dev branch AND on the canonical ligolang/ligo repository
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always

static-binary:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - nix-build nix -A ligo-static
    # Check that the binary is truly static and has 0 dependencies
    - test $(nix-store -q --references ./result | wc -l) -eq 0
    - cp -L result/bin/ligo ligo
    - chmod +rwx ligo
  artifacts:
    expose_as: "Static Linux binary"
    paths:
      - ligo

changelog:
  extends: .nix
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - nix-build nix -A ligo-changelog
    - cp -L result/* .
  artifacts:
    paths:
      - changelog.md
      - changelog.txt
      - release-notes.md
      - release-notes.txt

release:
  stage: push
  image: registry.gitlab.com/gitlab-org/release-cli
  rules:
    - if: '$CI_COMMIT_TAG =~ /[0-9]+\.[0-9]+\.[0-9]+/'
      when: always
  dependencies:
    - changelog
    - static-binary
    - deb
  ## FIXME find a better solution to upload the binary
  artifacts:
    expire_in: 1000 yrs
    paths:
      - ligo
      - ligo.deb
  script:
    - release-cli create --name "Release $CI_COMMIT_TAG" --description "$(cat release-notes.md)" --assets-links-name "Static Linux binary" --assets-links-url "$CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/raw/ligo" --assets-links-name "deb package" --assets-links-url "$CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/raw/ligo.deb"

.website:
  extends: .nix
  script:
    - nix-build nix -A ligo-website
    - cp -Lr --no-preserve=mode,ownership,timestamps result/ public
  artifacts:
    paths:
      - public

pages:
  extends: .website
  rules:
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always

pages-attempt:
  extends: .website
  only:
    - merge_requests
    - /^.*-run-dev$/
